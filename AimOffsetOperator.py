import bpy
from . AimOffset import AimOffsetUtils

class AimOffsetOperator(bpy.types.Operator):
    bl_idname = "view3d.aim_offset_operator"
    bl_label = "Simple operator"
    bl_description = "adds an extra bone in world space on the cursor's location"

    def execute(self, context):
        AimOffsetUtils.AimOffset(self, context)
        return {'FINISHED'}