import bpy
from . AutoBoneOrient import AutoBoneOrientUtils

class AutoBoneOrientOperator(bpy.types.Operator):
    bl_idname = "view3d.auto_bone_orient_operator"
    bl_label = "Simple operator"
    bl_description = "Creates basice FK rig on skeleton. Fixing orientation issues. Ideal for rigs coming from other 3D softwares "

    def execute(self, context):
        AutoBoneOrientUtils.find_correction_matrix(self, context)
        return {'FINISHED'}