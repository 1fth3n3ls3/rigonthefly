import bpy
from . AddExtraBone import AddExtraBoneUtils

class AddExtraBoneOperator(bpy.types.Operator):
    bl_idname = "view3d.add_extra_bone_operator"
    bl_label = "Simple operator"
    bl_description = "adds an extra bone in world space on the cursor's location"

    def execute(self, context):
        AddExtraBoneUtils.AddExtraBone(self, context)
        return {'FINISHED'}