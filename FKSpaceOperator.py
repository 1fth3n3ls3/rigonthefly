import bpy
from . FKSpace import FKSpaceUtils

class FKSpaceOperator(bpy.types.Operator):
    bl_idname = "view3d.fk_space_operator"
    bl_label = "Simple operator"
    bl_description = "Changes selected controllers containing .aim.rig in their name back to FK"

    def execute(self, context):
        FKSpaceUtils.FKSpace(self, context)
        return {'FINISHED'}