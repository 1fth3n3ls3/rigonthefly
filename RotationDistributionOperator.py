import bpy
from . RotationDistribution import RotationDistributionUtils

class RotationDistributionOperator(bpy.types.Operator):
    bl_idname = "view3d.rotation_distribution_operator"
    bl_label = "Simple operator"
    bl_description = "Distributes rotation linearly from active bone down hierarchy of selected controllers"

    def execute(self, context):
        RotationDistributionUtils.RotationDistribution(self, context)
        return {'FINISHED'}