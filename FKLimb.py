import bpy
from . Utility import StateUtility

class FKLimbUtils:

    def FKLimb (self, context):
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #add bone name to selectedBonesN to have it's generated IK controller selected at the end of the script
        selectedIKBonesN = list()
        for bone in bpy.context.selected_pose_bones:
            if ".IK.rig" in bone.name:
                selectedIKBonesN.append(bone.name)

        for ikHandleN in selectedIKBonesN:
            #find name of FK bone from IK bone selection
            FKTipBoneN = ikHandleN.replace(".IK.rig",".rig")
            #make list of FK bones for baking
            FKBonesList = FKTipBoneN, bpy.context.object.pose.bones[FKTipBoneN].parent.name, bpy.context.object.pose.bones[FKTipBoneN].parent.parent.name

            #change selection to FKBonesList
            bpy.ops.pose.select_all(action='DESELECT')
            for rigBone in FKBonesList:
                bpy.context.object.data.bones[rigBone].select = True

            #bake animation
            StateUtility.BakeAnimation()

            #show bone layer 3 to access hidden bones
            bpy.context.object.data.layers[2] = True
            #move FK bones to layer 2
            bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
            #hide back layer 3
            bpy.context.object.data.layers[2] = False

            #find relevant IK bones to remove
            IKBonesToRemove = FKBonesList[0].replace(".rig",".IK.rig"), FKBonesList[1].replace(".rig",".Pole.rig")

            #deselect all to
            bpy.ops.pose.select_all(action='DESELECT')
            #select bones to remove to remove their keyframes first
            for rigBone in IKBonesToRemove:
                bpy.context.object.data.bones[rigBone].select = True

            #clear all key frames of selected bones
            bpy.ops.anim.keyframe_clear_v3d()

            #remove IK bones
            StateUtility.SetEditMode()
            armature = bpy.context.object.data
            for bone in IKBonesToRemove:
                armature.edit_bones.remove(armature.edit_bones[bone])

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')
            bpy.ops.pose.select_all(action='DESELECT')
