import bpy
from . GroupSection import GroupSectionUtils

class GroupSectionOperator(bpy.types.Operator):
    bl_idname = "view3d.group_section_operator"
    bl_label = "Simple operator"
    bl_description = "groups selected chain of bones under a new bone"

    def execute(self, context):
        GroupSectionUtils.GroupSection(self, context)
        return {'FINISHED'}