import bpy
from . ParentSpaceCopy import ParentSpaceCopyUtils

class ParentSpaceCopyOperator(bpy.types.Operator):
    bl_idname = "view3d.parent_space_copy_operator"
    bl_label = "Simple operator"
    bl_description = "Parent selected controllers to a copy of the active controller"

    def execute(self, context):
        ParentSpaceCopyUtils.ParentSpaceCopy(self, context)        
        return {'FINISHED'}