# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "RigOnTheFly",
    "author" : "Dypsloom",
    "description" : "",
    "blender" : (2, 80, 0),
    "version" : (0, 0, 1),
    "location" : "View3D",
    "warning" : "",
    "category" : "Animation & Rig"
}

import bpy

from . AutoBoneOrientOperator import AutoBoneOrientOperator
from . RigOnSkeletonOperator import RigOnSkeletonOperator
from . BakeOnSkeletonOperator import BakeOnSkeletonOperator
from . BakeOrientOnSkeletonOperator import BakeOrientOnSkeletonOperator
from . ControllerSizePlusOperator import ControllerSizePlusOperator
from . ControllerSizeMinusOperator import ControllerSizeMinusOperator
from . IKLimbOperator import IKLimbOperator
from . IKLimbPoleAngleOperator import IKLimbPoleAngleOperator
from . FKLimbOperator import FKLimbOperator
from . RotationModeOperator import RotationModeOperator
from . InheritRotationOffOperator import InheritRotationOffOperator
from . InheritRotationOnOperator import InheritRotationOnOperator
from . InheritScaleOffOperator import InheritScaleOffOperator
from . InheritScaleOnOperator import InheritScaleOnOperator
from . RotationDistributionOperator import RotationDistributionOperator
from . ApplyDistributionOperator import ApplyDistributionOperator
from . GroupSectionOperator import GroupSectionOperator
from . WorldPositionOperator import WorldPositionOperator
from . RemoveWorldTransformsOperator import RemoveWorldTransformsOperator
from . IKChainOperator import IKChainOperator
from . AimWorldOperator import AimWorldOperator
from . StretchWorldOperator import StretchWorldOperator
from . AimChainOperator import AimChainOperator
from . StretchChainOperator import StretchChainOperator
from . FKSpaceOperator import FKSpaceOperator
from . ParentSpaceOperator import ParentSpaceOperator
from . ParentSpaceCopyOperator import ParentSpaceCopyOperator
from . RemoveParentSpaceOperator import RemoveParentSpaceOperator
from . AddExtraBoneOperator import AddExtraBoneOperator
from . DeleteBonesOperator import DeleteBonesOperator
from . TranslationInertiaOperator import TranslationInertiaOperator
from . RotationInertiaOperator import RotationInertiaOperator
from . ScaleInertiaOperator import ScaleInertiaOperator
from . RigOnTheFly import RigOnTheFly, RigBake, ControllerSize, IKFKSwitch, RotationScaleTools, ExtraBone, SpaceSwitch, InertiaOnTransforms, RotationModeMenu

classes = (
    AutoBoneOrientOperator,
    RigOnSkeletonOperator, 
    BakeOnSkeletonOperator,
    BakeOrientOnSkeletonOperator,
    ControllerSizePlusOperator, 
    ControllerSizeMinusOperator, 
    IKLimbOperator, 
    IKLimbPoleAngleOperator, 
    FKLimbOperator, 
    RotationModeOperator,
    InheritRotationOffOperator, 
    InheritScaleOffOperator, 
    InheritRotationOnOperator, 
    InheritScaleOnOperator,
    RotationDistributionOperator,
    ApplyDistributionOperator,
    GroupSectionOperator, 
    WorldPositionOperator,
    RemoveWorldTransformsOperator,
    IKChainOperator,
    AimWorldOperator,
    StretchWorldOperator,
    AimChainOperator,
    StretchChainOperator,   
    FKSpaceOperator, 
    ParentSpaceOperator, 
    ParentSpaceCopyOperator,
    RemoveParentSpaceOperator, 
    AddExtraBoneOperator, 
    DeleteBonesOperator,
    TranslationInertiaOperator, 
    RotationInertiaOperator, 
    ScaleInertiaOperator, 
    RigOnTheFly,
    RigBake,
    ControllerSize,
    IKFKSwitch,
    RotationScaleTools,
    ExtraBone,
    SpaceSwitch,
    InertiaOnTransforms,
    RotationModeMenu

)

register, unregister = bpy.utils.register_classes_factory(classes)