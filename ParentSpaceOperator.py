import bpy
from . ParentSpace import ParentSpaceUtils

class ParentSpaceOperator(bpy.types.Operator):
    bl_idname = "view3d.parent_space_operator"
    bl_label = "Simple operator"
    bl_description = "Parent selected controllers to the active controller"

    def execute(self, context):
        result = ParentSpaceUtils.ParentSpaceCondition(self, context)
        if result != None:
            self.report(*result) # * unpacks list into a tuple
            return {'CANCELLED'}

        ParentSpaceUtils.ParentSpace(self, context)

        return {'FINISHED'}