import bpy
from . IKLimb import IKLimbUtils

class IKLimbOperator(bpy.types.Operator):
    bl_idname = "view3d.ik_limb_operator"
    bl_label = "Simple operator"
    bl_description = "Changes selected controller and it's two parents to work in IK"

    def execute(self, context):
        IKLimbUtils.IKLimb(self, context)
        return {'FINISHED'}