import bpy
from . Utility import StateUtility

class RemoveWorldTransformsUtils:

    def RemoveWorldTransforms (self, context):
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #list selected bones names
        selectedBonesP = bpy.context.selected_pose_bones.copy()

        worldBonesN = list()

        for selectedBoneP in selectedBonesP:
            if ".world.rig" in selectedBoneP.name:
                worldBonesN.append(selectedBoneP.name)

        #show hidden bone layer 3 to access hidden rig bones
        bpy.context.object.data.layers[2] = True        

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')
        #select local bones related to previously selected world space bones
        for worldBoneN in worldBonesN:
            bpy.context.object.data.bones[worldBoneN.replace(".world.rig",".rig")].select =True
        
        #return affected rig bones to the main layer 2
        bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')
        #select local bones related to previously selected world space bones
        for worldBoneN in worldBonesN:
            bpy.context.object.data.bones[worldBoneN].select =True
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()        

        #hide layer 3 since it is no more needed
        bpy.context.object.data.layers[2] = False

        #remove selected world space bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for boneN in worldBonesN:
            armature.edit_bones.remove(armature.edit_bones[boneN])
        
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')
        #select local bones related to previously selected world space bones
        for worldBoneN in worldBonesN:
            bpy.context.object.data.bones[worldBoneN.replace(".world.rig",".rig")].select =True