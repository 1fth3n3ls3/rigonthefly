import bpy
from . Utility import StateUtility

class ApplyDistributionUtils:

    def ApplyDistribution (self, context):
        bonesToBakeN = list()
        bonesToRemoveN = list() #bones to delete at the end of the script

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #find .top.rig bones and sort bones to bake and bones to remove
        topRotBonesN = list()
        for bone in bpy.context.selected_pose_bones:
            if ".top.rig" in bone.name:
                topRotBonesN.append(bone.name)
                bonesToRemoveN.append(bone.name)
                bonesToBakeN.append(bone.name.replace(".top.rig",".rig"))
        
        #find .top.rig bones children and sort bones to bake and bones to remove
        for topRotBoneN in topRotBonesN:
            for topRotChild in bpy.context.object.pose.bones[topRotBoneN].children:
                bonesToBakeN.append(topRotChild.name.replace(".rotTop.rig",".rig"))

                bonesToRemoveN.append(topRotChild.name)
                bonesToRemoveN.append(topRotChild.name.replace(".rotTop.rig",".rotBase.rig"))
        
        bpy.ops.pose.select_all(action='DESELECT')

        #change bonesToBake to layer 2 and selects them for baking
        for boneN in bonesToBakeN:
            boneP = bpy.context.object.data.bones[boneN]
            boneP.layers[1]=True
            boneP.layers[2]=False
            boneP.select = True
        
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselect all to
        bpy.ops.pose.select_all(action='DESELECT')
        #select bones to remove to remove their keyframes first
        for boneN in bonesToRemoveN:
            bpy.context.object.data.bones[boneN].select = True

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #remove bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for boneN in bonesToRemoveN:
            armature.edit_bones.remove(armature.edit_bones[boneN])

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        