import bpy
from . Utility import StateUtility

class RotationDistributionUtils:

    def RotationDistribution (self, context):

        #force edit mode
        StateUtility.SetEditMode()

        #list selected bones and order them in edit mode
        selectedBonesList = bpy.context.selected_editable_bones.copy()
        selectedBonesList.sort(key = lambda x:len(x.parent_recursive))

        #add selected bones names to selectedBonesN and set aside the active bone as the topBone
        selectedBonesN = list()
        for bone in selectedBonesList:
            if bone.name == bpy.context.active_bone.name:
                topBoneN = bpy.context.active_bone.name
                topRotBoneN = topBoneN.replace(".rig",".top.rig")
            else: selectedBonesN.append(bone.name)

        baseBoneN = selectedBonesN[0]
        

        #selects and duplicates the second bone in the hierarchy of the original selection
        bpy.ops.armature.select_all(action='DESELECT')
        bpy.context.object.data.edit_bones[topBoneN].select=True
        bpy.context.object.data.edit_bones[topBoneN].select_head=True
        bpy.context.object.data.edit_bones[topBoneN].select_tail=True

        bpy.ops.armature.duplicate()

        bpy.context.object.data.edit_bones[topBoneN +".001"].parent = bpy.context.object.data.edit_bones[baseBoneN]
        bpy.context.object.data.edit_bones[topBoneN +".001"].name = topRotBoneN

        #selects bones inbetween the base bone and the top bone
        bpy.ops.armature.select_all(action='DESELECT')
        for i in range(1, len(selectedBonesN)):
            inbetweenBoneN = selectedBonesN[i]
            bpy.context.object.data.edit_bones[inbetweenBoneN].select=True
            bpy.context.object.data.edit_bones[inbetweenBoneN].select_head=True
            bpy.context.object.data.edit_bones[inbetweenBoneN].select_tail=True
        
        #duplicate inbetween bones twice. one copy will follow the base bone's rotation and the other copy will follow the top bone's rotation
        bpy.ops.armature.duplicate()
        bpy.ops.armature.duplicate()

        #add duplicated bones with .001 suffix to the selection. The duplicated bones with suffix .002 are already selected
        for i in range(1, len(selectedBonesN)):
            baseInbetweenBoneN = selectedBonesN[i] +".001"
            bpy.context.object.data.edit_bones[baseInbetweenBoneN].select=True
            bpy.context.object.data.edit_bones[baseInbetweenBoneN].select_head=True
            bpy.context.object.data.edit_bones[baseInbetweenBoneN].select_tail=True

        #rename duplicated bones with .rig.001 suffix to .rotBase.rig and duplicated bones with .rig.002 suffix to .rotTop.rig
        for bone in bpy.context.selected_editable_bones:
            if ".rig.001" in bone.name:
                bone.parent = bpy.context.object.data.edit_bones[baseBoneN]
                bone.name = bone.name.replace(".rig.001",".rotBase.rig")

            if ".rig.002" in bone.name:  
                bone.parent = bpy.context.object.data.edit_bones[topRotBoneN]
                bone.name = bone.name.replace(".rig.002",".rotTop.rig")


        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #add rotation contraints to duplicated bones so that they copy their originals .rig bones rotation
        for bone in bpy.context.selected_pose_bones:
            if ".rotBase.rig" in bone.name:
                bpy.context.object.data.bones[bone.name].use_inherit_rotation = True

                copyRotation = bone.constraints.new('COPY_ROTATION')
                copyRotation.target = bpy.context.object
                copyRotation.subtarget = bone.name.replace(".rotBase.rig",".rig")

            if ".rotTop.rig" in bone.name:
                bpy.context.object.data.bones[bone.name].use_inherit_rotation = True

                copyRotation = bone.constraints.new('COPY_ROTATION')
                copyRotation.target = bpy.context.object
                copyRotation.subtarget = bone.name.replace(".rotTop.rig",".rig")

        #selects topRotBone and have it display at topBone's location using the Octagon controller shape and locking it's translation
        bpy.context.object.data.bones[topRotBoneN].select = True
        topRotBoneP = bpy.context.object.pose.bones[topRotBoneN]
        topRotBoneP.custom_shape_transform = bpy.context.object.pose.bones[topBoneN]
        topRotBoneP.custom_shape = bpy.data.objects["Octagon"]
        topRotBoneP.lock_location[0] = True
        topRotBoneP.lock_location[1] = True
        topRotBoneP.lock_location[2] = True

        #have topRotBone follow topBone's rotation
        copyRotation = bpy.context.object.pose.bones[topRotBoneN].constraints.new('COPY_ROTATION')
        copyRotation.target = bpy.context.object
        copyRotation.subtarget = topBoneN
        
        
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #have original selected bones inbetween the baseBone and the topBone follow both its duplicate's rotations
        for i in range(1, len(selectedBonesN)):
            inbetweenBoneN = selectedBonesN[i]
            inbetweenBoneP = bpy.context.object.pose.bones[inbetweenBoneN]
            
            copyBaseRotation = inbetweenBoneP.constraints.new('COPY_ROTATION')
            copyBaseRotation.target = bpy.context.object
            copyBaseRotation.subtarget = selectedBonesN[i].replace(".rig",".rotBase.rig")

            copyTopRotation = inbetweenBoneP.constraints.new('COPY_ROTATION')
            copyTopRotation.target = bpy.context.object
            copyTopRotation.subtarget = selectedBonesN[i].replace(".rig",".rotTop.rig")
            copyTopRotation.influence = i/(len(selectedBonesN))

        #have the topBone follow the topRotBone's rotation
        copyRotation = bpy.context.object.pose.bones[topBoneN].constraints.new('COPY_ROTATION')
        copyRotation.target = bpy.context.object
        copyRotation.subtarget = topRotBoneN


        #move bones non relevant to animation to layer 3
        bpy.context.object.data.bones[topRotBoneN].select = False
        bpy.context.object.data.bones[topBoneN].select = True

        nonRelevantBones = bpy.context.selected_pose_bones.copy()
        for i in range(1, len(selectedBonesN)):
            nonRelevantBones.append(bpy.context.object.pose.bones[selectedBonesN[i]]) 

        #deselct all
        bpy.ops.pose.select_all(action='DESELECT')

        #hide layer 3
        for bone in nonRelevantBones:
            bpy.context.object.data.bones[bone.name].layers[2]=True
            bpy.context.object.data.bones[bone.name].layers[1]=False

        #select topRotBone
        bpy.context.object.data.bones[topRotBoneN].select = True



