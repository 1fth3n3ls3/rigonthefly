import bpy
from . Utility import StateUtility

class InheritRotationOffUtils:

    def InheritRotationOff (self, context):
        
        selectedBonesListN = StateUtility.TempBoneCopySelectedBones()

        #remove inherit rotation to original bone selection
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone].use_inherit_rotation = False

        StateUtility.SelectedBonesCopyTempBones(selectedBonesListN)