import bpy
from mathutils import Matrix, Euler, Vector
from . Utility import StateUtility
from . PolygonShapesUtility import PolygonShapes

class AutoBoneOrientUtils:


    def find_correction_matrix(self, parent_correction_inv=None):
        #add controller shapes if not already in the scene
        if bpy.data.objects.get("Square") is None:
            PolygonShapes.SquarePolygon(self)
        if bpy.data.objects.get("Circle") is None:
            PolygonShapes.CirclePolygon(self)
        if bpy.data.objects.get("Octagon") is None:
            PolygonShapes.OctagonPolygon(self)
        if bpy.data.objects.get("Locator") is None:
            PolygonShapes.LocatorPolygon(self)

        #set aside the armature as a variable
        armature = bpy.context.object

        #set aside current frame to come back to it at the end of the script
        currentFrame = bpy.context.scene.frame_current

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        #select all base armature bones
        bpy.ops.pose.select_all(action='SELECT')
        #go to frame 0 and key the rig in bind pose
        bpy.context.scene.frame_set(0)
        for orientBone in bpy.context.selected_pose_bones:
            orientBone.location = (0,0,0)
            orientBone.rotation_quaternion = (1,0,0,0)
            orientBone.scale = (1,1,1)
        bpy.ops.anim.keyframe_insert_menu(type='WholeCharacter')

        #add bone groups
        armature.pose.bone_groups.new(name="Base")
        armature.pose.bone_groups['Base'].color_set = 'THEME09'
        armature.pose.bone_groups.new(name="Right")
        armature.pose.bone_groups['Right'].color_set = 'THEME01'
        armature.pose.bone_groups.new(name="Left")
        armature.pose.bone_groups['Left'].color_set = 'THEME04'

        leftSide = ["left","_l","l_",".l","l.","-l","l-"," left","left "]
        rightSide = ["right","_r","r_",".r","r.","-r","r-"," right","right "]

        for poseBone in bpy.context.selected_pose_bones:
            if any(poseBone.name.casefold().startswith(left) or poseBone.name.casefold().endswith(left) for left in leftSide):
                poseBone.bone_group_index = 2
            elif any(poseBone.name.casefold().startswith(right) or poseBone.name.casefold().endswith(right) for right in rightSide):
                poseBone.bone_group_index = 1
            else:
                poseBone.bone_group_index = 0
        
        #force edit mode
        StateUtility.SetEditMode()

        #select base armature
        bpy.ops.armature.select_all(action='DESELECT')
        bpy.ops.armature.select_all(action='SELECT')

        armatureName = bpy.context.active_object.name

        #list base bones
        baseBonesE = list(bpy.context.selected_editable_bones)
        baseBonesE.sort(key = lambda x:len(x.parent_recursive))
        baseBonesNames = list()
        for baseBoneE in baseBonesE:
            baseBonesNames.append(baseBoneE.name)

        for baseBone in bpy.context.selected_editable_bones:
            baseBone.use_connect = False

        #duplicate base armature. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #add .orient suffix to duplicate bones now known as orient bones and add it to orientBonesNames list.
        orientBonesNames= list()
        for orientBone in bpy.context.selected_editable_bones:            
            orientBone.name = orientBone.name.replace(".001",".orient")
            orientBonesNames.append(orientBone.name)
        
        parent_correction_inv = Matrix()

        #orient duplicated bones to be compatible with Rig on the Fly tools
        for orientBoneName in orientBonesNames:

            orientBone = bpy.data.armatures[armatureName].edit_bones[orientBoneName]

            from bpy_extras.io_utils import axis_conversion
            
            #if parent_correction_inv:
            #    orientBone.pre_matrix = parent_correction_inv @ (orientBone.pre_matrix if orientBone.pre_matrix else Matrix())

            correction_matrix = Matrix()

            # find best orientation to align baseBone with
            bone_children = tuple(child for child in orientBone.children)
            if len(bone_children) == 0:
                # no children, inherit the correction from parent (if possible)
                correction_matrix = parent_correction_inv
                #if orientBone.parent:
                #    correction_matrix = parent_correction_inv.inverted() if parent_correction_inv else None
            else:
                # else find how best to rotate the baseBone to align the Y axis with the children
                best_axis = (1, 0, 0)
                if len(bone_children) == 1:
                    childMatrix = bone_children[0].matrix
                    orientBoneMatrix = orientBone.matrix                    
                    orientBoneMatrixInv = orientBoneMatrix.inverted()                    
                    vec= orientBoneMatrixInv @ childMatrix                    
                    vec= vec.to_translation()

                    best_axis = Vector((0, 0, 1 if vec[2] >= 0 else -1))
                    if abs(vec[0]) > abs(vec[1]):
                        if abs(vec[0]) > abs(vec[2]):
                            best_axis = Vector((1 if vec[0] >= 0 else -1, 0, 0))
                    elif abs(vec[1]) > abs(vec[2]):
                        best_axis = Vector((0, 1 if vec[1] >= 0 else -1, 0))
                else:
                    # get the child directions once because they may be checked several times
                    child_locs = list()
                    for child in bone_children:
                        childMatrix = child.matrix
                        orientBoneMatrix = orientBone.matrix                    
                        orientBoneMatrixInv = orientBoneMatrix.inverted()                        
                        vec= orientBoneMatrixInv @ childMatrix                        
                        vec= vec.to_translation()
                        child_locs.append(vec)
                    child_locs = tuple(loc.normalized() for loc in child_locs if loc.magnitude > 0.0)

                    # I'm not sure which one I like better...                
                    best_angle = -1.0
                    for vec in child_locs:

                        test_axis = Vector((0, 0, 1 if vec[2] >= 0 else -1))
                        if abs(vec[0]) > abs(vec[1]):
                            if abs(vec[0]) > abs(vec[2]):
                                test_axis = Vector((1 if vec[0] >= 0 else -1, 0, 0))
                        elif abs(vec[1]) > abs(vec[2]):
                            test_axis = Vector((0, 1 if vec[1] >= 0 else -1, 0))

                        # find max angle to children
                        max_angle = 1.0
                        for loc in child_locs:
                            max_angle = min(max_angle, test_axis.dot(loc))

                        # is it better than the last one?
                        if best_angle < max_angle:
                            best_angle = max_angle
                            best_axis = test_axis                

                # convert best_axis to axis string
                to_up = 'Z' if best_axis[2] >= 0 else '-Z'
                if abs(best_axis[0]) > abs(best_axis[1]):
                    if abs(best_axis[0]) > abs(best_axis[2]):
                        to_up = 'X' if best_axis[0] >= 0 else '-X'
                elif abs(best_axis[1]) > abs(best_axis[2]):
                    to_up = 'Y' if best_axis[1] >= 0 else '-Y'
                to_forward = 'X' if to_up not in {'X', '-X'} else 'Y'

                # Build correction matrix
                #if (to_up, to_forward) != ('Y', 'X'):
                correction_matrix = axis_conversion(from_forward='X',
                                                    from_up='Y',
                                                    to_forward=to_forward,
                                                    to_up=to_up,
                                                    ).to_4x4()
                            
            orientBone.matrix = orientBone.matrix @ correction_matrix
            parent_correction_inv = correction_matrix

        #now the orient bones are well oriented!

        #duplicate orient bones. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #add .rig suffix to duplicate bones now known as orient bones and add it to orientBonesNames list.
        TEMPorientBonesNames= list()
        for TEMPorientBone in bpy.context.selected_editable_bones:      
            TEMPorientBonesNames.append(TEMPorientBone.name)

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')        

        #make temporary orient bones follow base armature's animation with offset on rotation+scale.
        for TEMPorientBone in bpy.context.selected_pose_bones:

            bpy.context.object.data.bones[TEMPorientBone.name].use_inherit_rotation = False
            bpy.context.object.data.bones[TEMPorientBone.name].inherit_scale = 'NONE'

            childOf = TEMPorientBone.constraints.new('CHILD_OF')
            childOf.target = bpy.context.object
            childOf.subtarget = TEMPorientBone.basename
            
            TEMPorientBone = bpy.context.active_object.pose.bones[TEMPorientBone.name]

            context_copy = bpy.context.copy()
            context_copy["constraint"] = TEMPorientBone.constraints["Child Of"]
            bpy.context.active_object.data.bones.active = TEMPorientBone.bone
            bpy.ops.constraint.childof_set_inverse(context_copy, constraint="Child Of", owner='BONE')

            copyLocation = TEMPorientBone.constraints.new('COPY_LOCATION')
            copyLocation.target = bpy.context.object
            copyLocation.subtarget = TEMPorientBone.basename
        
        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselect temporary orient bones
        bpy.ops.pose.select_all(action='DESELECT')
        #select orient bone list
        for orientBone in orientBonesNames:
            bpy.context.object.data.bones[orientBone].select = True

        #make selected bones follow duplicated bones with copy transform constraint
        for orientBone in bpy.context.selected_pose_bones:
            copyTransforms = orientBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = orientBone.name.replace(".orient",".orient.001")

        StateUtility.BakeAnimation()

        #remove copied bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for orientBone in orientBonesNames:
            armature.edit_bones.remove(armature.edit_bones[orientBone +".001"])

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')


        bpy.ops.pose.select_all(action='DESELECT')
        #reverse constraints. make base armature bones follow orient bones animation with offset on rotation+scale.
        for baseBoneName in baseBonesNames:
            bpy.context.object.data.bones[baseBoneName].select = True
            baseBone = bpy.context.active_object.pose.bones[baseBoneName]

            bpy.context.object.data.bones[baseBoneName].use_inherit_rotation = False
            bpy.context.object.data.bones[baseBoneName].inherit_scale = 'NONE'

            childOf = baseBone.constraints.new('CHILD_OF')
            childOf.target = bpy.context.object
            childOf.subtarget = baseBone.name +".orient"
            
            context_copy = bpy.context.copy()
            context_copy["constraint"] = baseBone.constraints["Child Of"]
            bpy.context.active_object.data.bones.active = baseBone.bone
            bpy.ops.constraint.childof_set_inverse(context_copy, constraint="Child Of", owner='BONE')

            copyLocation = baseBone.constraints.new('COPY_LOCATION')
            copyLocation.target = bpy.context.object
            copyLocation.subtarget = baseBone.name +".orient"

        #delete keys on base bones using script inspired from https://blenderartists.org/t/quick-delete-keyframes-i-need-help-to-modification-script/645987
        frameStart=bpy.context.active_object.animation_data.action.frame_range.x
        frameEnd=bpy.context.active_object.animation_data.action.frame_range.y

        armature = bpy.context.object
        action = armature.animation_data.action
        delete = []

        # get selected bones names
        selectedBaseBones = [b.name for b in armature.data.bones if b.select]

        # get bone names from fcurve data_path
        for fCurve in action.fcurves:
            name = fCurve.data_path.split(sep='"', maxsplit=2)[1]

            # check if bone is selected and got keyframes in range
            if name in selectedBaseBones:
                for i in fCurve.keyframe_points:
                    if frameStart < i.co[0] < frameEnd+1:
                        delete.append((fCurve.data_path, i.co[0]))

        # delete keyframes
        for i in delete:
            armature.keyframe_delete(i[0], index=-1, frame=i[1])
        
        #sets the current frame of the scene to what it was before starting the script
        bpy.context.scene.frame_set(currentFrame)

        #move rig bones to the second armature layer. 
        bpy.ops.pose.bone_layers(layers=(False, False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        
        #orient bones are set up. now time to add the rig bones

        #force edit mode
        StateUtility.SetEditMode()

        bpy.context.object.data.layers[3] = True

        #select all orient bones
        bpy.ops.armature.select_all(action='DESELECT')
        bpy.context.object.data.layers[3] = False
        bpy.ops.armature.select_all(action='SELECT')

        for orientBone in bpy.context.selected_editable_bones:
            orientBone.use_connect = False
            
        #duplicate orient bones. Duplicated bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #add .rig suffix to orient bones now known as rig bones.
        for copiedBone in bpy.context.selected_editable_bones: 
            copiedBone.name = copiedBone.name.replace(".001",".rig")

        #move rig bones to the second armature layer. 
        bpy.ops.armature.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        #make second armature layer visible and first layer hidden, to show only rig bones.
        bpy.context.object.data.layers[1] = True
        bpy.context.object.data.layers[0] = False

        #armature is in pose mode
        bpy.ops.object.mode_set(mode='POSE')
        

        #change rig bones' display to circle, rotation mode to euler YZX and adds copy transform constraint to copy the base armature's animation.
        for rigBone in bpy.context.selected_pose_bones:
            rigBone.custom_shape = bpy.data.objects["Circle"]
            bpy.context.object.data.bones[rigBone.name].show_wire = True
            rigBone.rotation_mode = 'YZX'
            copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = rigBone.name.replace(".rig","")
        
        StateUtility.BakeAnimation()

        

        #deselect all rig bones
        bpy.ops.pose.select_all(action='TOGGLE')

        #display base armature layer and hide rig armature layer 
        bpy.context.object.data.layers[0] = True
        bpy.context.object.data.layers[1] = False

        #select base armature
        bpy.ops.pose.select_all(action='SELECT')

        #base armature now follows rig armature
        for orientBone in bpy.context.selected_pose_bones:
            copyTransforms = orientBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = orientBone.name + ".rig"

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #deselect base armature
        bpy.ops.pose.select_all(action='DESELECT')

        #show rig armature
        bpy.context.object.data.layers[1] = True
        bpy.context.object.data.layers[0] = False
