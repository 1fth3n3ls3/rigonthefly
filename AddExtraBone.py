import bpy
from . Utility import StateUtility
#from . AddExtraBone import AddExtraBoneUtils

class AddExtraBoneUtils:

    def AddExtraBone (self, context):
        #force edit mode
        StateUtility.SetEditMode()

        #add a bone of 1 unit tall at the cursor's location
        bpy.ops.armature.bone_primitive_add()

        armature = bpy.context.selected_objects[0]

        armature.data.edit_bones['Bone'].select = True
        armature.data.edit_bones['Bone'].select_head = True
        armature.data.edit_bones['Bone'].select_tail = True

        #find the matrix coordinates of the armature object
        armatureMatrix = bpy.context.object.matrix_world
        #invert armature's matrix to find where global(0,0,0) is in relation to the armature's position/roation
        armatureMatrixInvert= armatureMatrix.copy()
        armatureMatrixInvert.invert()
        #set aim bone position to global (0,0,0) with axis following world's
        bpy.context.selected_editable_bones[0].matrix = armatureMatrixInvert
            

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #select new extra bone to change it's custom shape and viewport display
        newBone = bpy.context.object.data.bones['Bone']
        
        newBone.select=True
        bpy.context.object.pose.bones['Bone'].custom_shape = bpy.data.objects["Locator"]
        newBone.show_wire=True

        #set bone group of new extra bone to Base layer
        bpy.context.selected_pose_bones[0].bone_group_index = 0

        #rename new bone
        newBone.name = AddExtraBoneUtils.ExtraBoneName(1)


    @staticmethod
    def ExtraBoneName (count):
        boneName = "extra"+str(count)+".rig"

        if bpy.context.object.pose.bones.get(boneName)==None:
            return boneName
        else:
            return AddExtraBoneUtils.ExtraBoneName(count+1)