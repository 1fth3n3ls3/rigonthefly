import bpy
from . RigOnSkeleton import RigOnSkeletonUtils

class RigOnSkeletonOperator(bpy.types.Operator):
    bl_idname = "view3d.rig_on_skeleton_operator"
    bl_label = "Simple operator"
    bl_description = "Adds basic FK rig controllers to skeleton"

    def execute(self, context):
        RigOnSkeletonUtils.RigOnSkeleton(self, context)
        return {'FINISHED'}