import bpy

class PolygonShapes:

    def SquarePolygon (self):
       view_layer = bpy.context.view_layer
       verts = [(1,0,1),(-1,0,1),(-1,0,-1),(1,0,-1)]
       faces = [(0,1,2,3)]

       mySquareMesh = bpy.data.meshes.new("Square")
       mySquareObject = bpy.data.objects.new("Square", mySquareMesh)

       mySquareObject.location = (0,0,0)
       view_layer.active_layer_collection.collection.objects.link(mySquareObject)

       mySquareMesh.from_pydata(verts,[],faces)
       mySquareMesh.update(calc_edges=True)
       bpy.data.objects["Square"].hide_set(True)

    def CirclePolygon (self):

        view_layer = bpy.context.view_layer
        verts = [(0,0,1.3),(0.19509,0,0.98079),(0.38268,0,0.92388),(0.55557,0,0.83147),(0.70711,0,0.70711),(0.83147,0,0.55557),(0.92388,0,0.38268),(0.98079,0,0.19509),(1,0,0),(0.98079,0,-0.19509),(0.92388,0,-0.38268),(0.83147,0,-0.55557),(0.70711,0,-0.70711),(0.55557,0,-0.83147),(0.38268,0,-0.92388),(0.19509,0,-0.98079),(0,0,-0.8),(-0.19509,0,-0.98079),(-0.38268,0,-0.92388),(-0.55557,0,-0.83147),(-0.70711,0,-0.70711),(-0.83147,0,-0.55557),(-0.92388,0,-0.38268),(-0.98079,0,-0.19509),(-1,0,0),(-0.98079,0,0.19509),(-0.92388,0,0.38268),(-0.83147,0,0.55557),(-0.70711,0,0.70711),(-0.55557,0,0.83147),(-0.38268,0,0.92388),(-0.19509,0,0.98079)]
        faces = [(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31)]

        myCircleMesh = bpy.data.meshes.new("Circle")
        myCircleObject = bpy.data.objects.new("Circle", myCircleMesh)

        myCircleObject.location = (0,0,0)
        view_layer.active_layer_collection.collection.objects.link(myCircleObject)

        myCircleMesh.from_pydata(verts,[],faces)
        myCircleMesh.update(calc_edges=True)
        bpy.data.objects["Circle"].hide_set(True)

    def OctagonPolygon (self):
        view_layer = bpy.context.view_layer
        verts = [(0,0,1.3),(0.19509,0,0.98079),(0.70711,0,0.70711),(1,0,0),(0.70711,0,-0.70711),(0,0,-1),(-0.70711,0,-0.70711),(-1,0,0),(-0.70711,0,0.70711),(-0.19509,0,0.98079)]
        faces = [(0,1,2,3,4,5,6,7,8,9)]

        myOctagonMesh = bpy.data.meshes.new("Octagon")
        myOctagonObject = bpy.data.objects.new("Octagon", myOctagonMesh)

        myOctagonObject.location = (0,0,0)
        view_layer.active_layer_collection.collection.objects.link(myOctagonObject)

        myOctagonMesh.from_pydata(verts,[],faces)
        myOctagonMesh.update(calc_edges=True)
        bpy.data.objects["Octagon"].hide_set(True)

    def LocatorPolygon (self):
        view_layer = bpy.context.view_layer
        verts = [(0,0,0),(0,0,1),(0,0,-1),(1,0,0),(-1,0,0),(0,1,0),(0,-1,0)]
        faces = [(0,1,2),(0,3,4),(0,5,6)]

        myLocatorMesh = bpy.data.meshes.new("Locator")
        myLocatorObject = bpy.data.objects.new("Locator", myLocatorMesh)

        myLocatorObject.location = (0,0,0)
        view_layer.active_layer_collection.collection.objects.link(myLocatorObject)

        myLocatorMesh.from_pydata(verts,[],faces)
        myLocatorMesh.update(calc_edges=True)

