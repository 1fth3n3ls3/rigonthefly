import bpy
from . DeleteBones import DeleteBonesUtils

class DeleteBonesOperator(bpy.types.Operator):
    bl_idname = "view3d.delete_bones_operator"
    bl_label = "Simple operator"
    bl_description = "delete selected bones"

    def execute(self, context):
        DeleteBonesUtils.DeleteBones(self, context)
        return {'FINISHED'}