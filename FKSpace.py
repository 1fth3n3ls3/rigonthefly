import bpy
from . Utility import StateUtility
from . InheritRotationOn import InheritRotationOnUtils
from . InheritScaleOn import InheritScaleOnUtils

class FKSpaceUtils:

    def FKSpace (self, context):

        inheritedRotationListN = list()
        inheritedScaleListN = list()
        stretchChainListN = list()

        #list selected aim bones
        selectedRigBonesListP = list(bpy.context.selected_pose_bones)
        selectedAimBonesNameList = list()
        for selectedBone in selectedRigBonesListP:
            if ".aim.rig" in selectedBone.name:
                selectedAimBonesNameList.append(selectedBone.name)

                #find selected bone that are a result of Stretch Chain
                if selectedBone.constraints.get('Stretch To') is not None:
                    #find the subtarget on each bone of a Stretch Chain
                    subtargetN = selectedBone.constraints.get('Stretch To').subtarget
                    #find bone that need it's inherit rotation and scale turned off to prevent baking errors
                    selectedBoneInherit = bpy.context.object.data.bones[subtargetN.replace(".aim.rig",".rig")]

                    #set aside .rig of aim.rig subtarget of stretch chains
                    stretchChainListN.append(subtargetN.replace(".aim.rig",".rig"))

                    #turn off inherit rotation and scale of bones affected by stretch chain
                    selectedBoneInherit.use_inherit_rotation = False
                    selectedBoneInherit.use_inherit_scale = False

                    #set aside bones which got their inherit rotation and scale turned off
                    if selectedBoneInherit.use_inherit_rotation == True:
                        inheritedRotationListN.append(subtargetN.replace(".aim.rig",".rig"))

                    if selectedBoneInherit.use_inherit_scale == True:
                        inheritedScaleListN.append(subtargetN.name.replace(".aim.rig",".rig"))        

        #show hidden bone layer 3 to access hidden rig bones
        bpy.context.object.data.layers[2] = True

        #deselect all bones
        bpy.ops.pose.select_all(action='DESELECT')

        #select rig bones from aim bones list
        for bone in selectedAimBonesNameList:
            bpy.context.object.data.bones[bone.replace(".aim.rig",".rig")].select = True

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()        

        #deselect all to
        bpy.ops.pose.select_all(action='DESELECT')
        #select bones to remove, to remove their keyframes first
        for rigBone in selectedAimBonesNameList:
            bpy.context.object.data.bones[rigBone].select = True

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #remove selected aim bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for aimBone in selectedAimBonesNameList:
            armature.edit_bones.remove(armature.edit_bones[aimBone])
        
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        
        bpy.ops.pose.select_all(action='DESELECT')

        #check if there are any stretch chain selected to reapply inherit rotation and scale
        if len(stretchChainListN) > 0:
            #reapply inherit rotation if it was active before launching the script
            if len(inheritedRotationListN) > 0:
                for boneN in inheritedRotationListN:
                    bpy.context.object.data.bones[boneN].select = True
                InheritRotationOnUtils.InheritRotationOn            
            bpy.ops.pose.select_all(action='DESELECT')

            #reapply inherit scale if it was active before launching the script
            if len(inheritedScaleListN) > 0:
                for boneN in inheritedScaleListN:
                    bpy.context.object.data.bones[boneN].select = True
                InheritScaleOnUtils.InheritScaleOn
            bpy.ops.pose.select_all(action='DESELECT')
        
        #select rig bones to move them to bone layer 2
        for aimBoneN in selectedAimBonesNameList:
            bpy.context.object.data.bones[aimBoneN.replace(".aim.rig",".rig")].select = True        

        bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
        #hide bone layer 3
        bpy.context.object.data.layers[2] = False
