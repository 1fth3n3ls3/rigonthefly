import bpy
import math
from . Utility import StateUtility

class IKLimbUtils:

    def IKLimb (self, context):

        #add bone name to selectedBonesN to have it's generated IK controller selected at the end of the script
        selectedBonesN = list()
        for bone in bpy.context.selected_pose_bones:
            selectedBonesN.append(bone.name)

        for targetBoneN in selectedBonesN:
            #force edit mode
            StateUtility.SetEditMode()

            #get the name of the parent and parent's parent of one of the selected bones
            poleBoneN = bpy.context.object.pose.bones[targetBoneN].parent.name
            baseBoneN = bpy.context.object.pose.bones[targetBoneN].parent.parent.name

            #deselect all
            bpy.ops.armature.select_all(action='DESELECT')

            #selects and duplicates the last bone in the hierarchy of the original selection
            bpy.context.object.data.edit_bones[targetBoneN].select=True
            bpy.context.object.data.edit_bones[targetBoneN].select_head=True
            bpy.context.object.data.edit_bones[targetBoneN].select_tail=True
            bpy.ops.armature.duplicate()
            #rename ik bone
            bpy.context.object.data.edit_bones[targetBoneN +".001"].name = targetBoneN.replace(".rig",".IK.rig")

            ikTargetBoneN = targetBoneN.replace(".rig",".IK.rig")
            #remove parent
            bpy.context.selected_editable_bones[0].parent = None

            bpy.ops.armature.select_all(action='DESELECT')
            #selects and duplicates the second bone in the hierarchy of the original selection
            bpy.context.object.data.edit_bones[poleBoneN].select=True
            bpy.context.object.data.edit_bones[poleBoneN].select_head=True
            bpy.context.object.data.edit_bones[poleBoneN].select_tail=True
            bpy.ops.armature.duplicate()
            #rename ik bone
            bpy.context.selected_editable_bones[0].name = bpy.context.selected_editable_bones[0].name.replace(".rig.001",".Pole.rig")

            ikPoleBoneN = bpy.context.selected_editable_bones[0].name
            #remove parent
            bpy.context.selected_editable_bones[0].parent = None

            #snap tail of selectedPoleBoneN to ikTargetBoneN head's position
            bpy.context.object.data.edit_bones[poleBoneN].tail = bpy.context.object.data.edit_bones[ikTargetBoneN].head

            #reorient pole bone to +y or -y only
            headPosition = bpy.context.object.data.edit_bones[ikPoleBoneN].head
            bpy.context.object.data.edit_bones[ikPoleBoneN].tail = (headPosition[0],headPosition[1]+1,headPosition[2])
            bpy.context.object.data.edit_bones[ikPoleBoneN].roll = 0

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')
            #change rig bones' display to square, rotation mode to euler YZX and adds copy transform constraint to copy the base armature's animation.
            ikTargetBone = bpy.context.object.pose.bones[ikTargetBoneN]
            ikTargetBone.custom_shape = bpy.data.objects["Square"]
            bpy.context.object.data.bones[ikTargetBone.name].show_wire = True
            ikTargetBone.rotation_mode = 'YZX'
            copyTransforms = ikTargetBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = ikTargetBone.name.replace(".IK.rig",".rig")


            ikPoleBone = bpy.context.object.pose.bones[ikPoleBoneN]
            ikPoleBone.custom_shape = bpy.data.objects["Locator"]
            bpy.context.object.data.bones[ikPoleBone.name].show_wire = True
            ikPoleBone.rotation_mode = 'YZX'
            copyTransforms = ikPoleBone.constraints.new('COPY_LOCATION')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = ikPoleBone.name.replace(".Pole.rig",".rig")

            #only adds ikTargetBoneN to selection since ikPoleBone is already selected
            bpy.context.object.data.bones[ikTargetBoneN].select = True

            #bake animation on selection and remove constraints
            StateUtility.BakeAnimation()

            #adds ik constraint to selectedPoleBoneN
            ikBone = bpy.context.object.pose.bones[poleBoneN]
            ik = ikBone.constraints.new('IK')
            ik.target = bpy.context.object
            ik.subtarget = ikTargetBoneN
            ik.pole_target = bpy.context.object
            ik.pole_subtarget = ikPoleBoneN
            ik.pole_angle = -math.pi/2 #-90°
            ik.chain_count = 2

            #selectedTargetBone follow ikTargetBone transforms
            selectedTargetBone = bpy.context.object.pose.bones[targetBoneN]
            copyTransforms = selectedTargetBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = selectedTargetBone.name.replace(".rig",".IK.rig")

            #deselect all to prevent baking bones that were left selected
            bpy.ops.pose.select_all(action='DESELECT')

            bpy.context.object.data.bones[targetBoneN].select = True
            bpy.context.object.data.bones[poleBoneN].select = True
            bpy.context.object.data.bones[baseBoneN].select = True

            #clear all key frames of selected bones
            bpy.ops.anim.keyframe_clear_v3d()

            #deselect all to prevent baking bones that were left selected
            bpy.ops.pose.select_all(action='DESELECT')

            #move non relevant bones to layer 3
            bpy.context.object.data.bones[targetBoneN].layers[2]=True
            bpy.context.object.data.bones[targetBoneN].layers[1]=False

            bpy.context.object.data.bones[poleBoneN].layers[2]=True
            bpy.context.object.data.bones[poleBoneN].layers[1]=False

            bpy.context.object.data.bones[baseBoneN].layers[2]=True
            bpy.context.object.data.bones[baseBoneN].layers[1]=False            

        #end script with new ik handles selected
        for targetBoneN in selectedBonesN:
            bpy.context.object.data.bones[targetBoneN.replace(".rig",".IK.rig")].select = True
