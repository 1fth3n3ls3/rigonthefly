import bpy
from . Utility import StateUtility

class ParentSpaceCopyUtils:

    def ParentSpaceCopy (self, context):

        #force edit mode
        StateUtility.SetEditMode()

        #duplicate rig bones to be used as aim bones
        bpy.ops.armature.duplicate()

        #list selected bones and order them in edit mode
        selectedBonesList = bpy.context.selected_editable_bones.copy()

        #get the name of the active bone, usually the last selected bone, and remove it's parent
        parentBoneE = bpy.context.active_bone
        parentBoneE.parent = None

        #create a list to contain the name of the duplicated bones 
        childBonesListN = []

        #for duplicates of selected, rename and parent to the active bone
        for bone in selectedBonesList:
            if bone.name != parentBoneE.name:
                bone.name = bone.name.replace(".rig.001",".child.rig")
                childBonesListN.append(bone.name)
                bone.parent = parentBoneE
        
        #rename active bone
        parentBoneE.name = parentBoneE.name.replace(".rig.001",".parent.rig")
        parentBoneN = parentBoneE.name

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #change child bones' display to octagon, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        for boneN in childBonesListN:
            bone = bpy.context.object.pose.bones[boneN]
            bone.custom_shape = bpy.data.objects["Octagon"]
            bone.custom_shape_scale *= 1.5
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = boneN.replace(".child.rig",".rig")            

        #change parent bone's display to square, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        parentBoneP = bpy.context.object.pose.bones[parentBoneN]
        parentBoneP.custom_shape = bpy.data.objects["Octagon"]
        parentBoneP.custom_shape_scale *= 1.5
        copyTransforms = parentBoneP.constraints.new('COPY_TRANSFORMS')
        copyTransforms.target = bpy.context.object
        copyTransforms.subtarget = parentBoneN.replace(".parent.rig",".rig")

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')

        #initially selected bones follow child bones
        for childBoneN in childBonesListN:
            rigBone = bpy.context.object.pose.bones[childBoneN.replace(".child.rig",".rig")]
            copyTransforms = rigBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = childBoneN
            #select initial rig bones to switch them to hidden layer 3 later
            bpy.context.object.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
        
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()
        
        bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        #deselects all
        bpy.ops.pose.select_all(action='DESELECT')
        #end with new parent bone selected
        bpy.context.object.data.bones[parentBoneN].select = True