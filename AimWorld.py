import bpy
from . Utility import StateUtility

class AimWorldUtils:
    
    @staticmethod
    def RemoveEditableBone(boneToRemove):
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in armature.edit_bones:
            if bone.name == boneToRemove.name:
                armature.edit_bones.remove(bone)

    def ChangeToAimWorld (self, context):
        print("start Aim World")

        #force edit mode
        StateUtility.SetEditMode()

        selectedRigBonesListE = list(bpy.context.selected_editable_bones)
        selectedRigBonesListE.sort(key = lambda x:len(x.parent_recursive))
        selectedRigBoneNameList = list()
        for selectedBone in selectedRigBonesListE:
            selectedRigBoneNameList.append(selectedBone.name)
        
        #duplicate rig bones to be used as aim bones
        bpy.ops.armature.duplicate()
       
        #add .aim.rig suffix to duplicate bones now known as aim bones.
        for copiedBones in bpy.context.selected_editable_bones:
            copiedBones.name = copiedBones.name.replace(".rig.001",".aim.rig")

            #find the matrix coordinates of the armature object
            armatureMatrix = bpy.context.object.matrix_world
            #invert armature's matrix to find where global(0,0,0) is in relation to the armature's position/roation
            armatureMatrixInvert= armatureMatrix.copy()
            armatureMatrixInvert.invert()
            #set aim bone position to global (0,0,0) with axis following world's
            copiedBones.matrix = armatureMatrixInvert

        #unparent aim bones for world space translation
        aimBonesListE = bpy.context.selected_editable_bones.copy()
        aimBoneNameList = list()
        for aimBone in aimBonesListE:
            aimBoneNameList.append(aimBone.name)
            aimBone.parent = None


        #armature set to pose mode
        bpy.ops.object.mode_set(mode='POSE')

        aimBonesListN = list()
        for bone in selectedRigBoneNameList:
            aimBonesListN.append(bone.replace(".rig",".aim.rig"))

        #change rig bones' display to locator and adds copy location constraint to copy the rig bones tail animation.
        for aimBoneN in aimBonesListN:
            aimBone = bpy.context.object.pose.bones[aimBoneN]
            aimBone.custom_shape = bpy.data.objects["Locator"]
            bpy.context.object.data.bones[aimBoneN].show_wire = True
            copyLocation = aimBone.constraints.new('COPY_LOCATION')
            copyLocation.target = bpy.context.object
            copyLocation.subtarget = aimBoneN.replace(".aim.rig",".rig")
            copyLocation.head_tail = 0.001

            #and add limit distance constraint so that all aim bones are at the same distance away from the rig bone
            limitDistance = aimBone.constraints.new('LIMIT_DISTANCE')
            limitDistance.target = bpy.context.object
            limitDistance.subtarget = aimBoneN.replace(".aim.rig",".rig")
            limitDistance.limit_mode = 'LIMITDIST_OUTSIDE'
            aimDistance = bpy.context.object.aimDistance
            limitDistance.distance = aimDistance

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #make rig bones follow coresponding aim bones
        for aimBoneN in aimBonesListN:
            ikConstraint = bpy.context.object.pose.bones[aimBoneN.replace(".aim.rig",".rig")].constraints.new('IK')
            ikConstraint.target = bpy.context.object
            ikConstraint.subtarget = aimBoneN
            ikConstraint.chain_count = 1
