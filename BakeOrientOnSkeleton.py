import bpy
from . Utility import StateUtility

class BakeOrientOnSkeletonUtils:
    def BakeOrientOnSkeleton (self, context):

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #show all layers
        for i in range(0,32):
            bpy.context.object.data.layers[i] = True

        for bone in bpy.context.object.data.bones:
            if not ".orient" in bone.name:
                bpy.context.object.data.bones[bone.name].select = True

        #bake skin bones
        StateUtility.BakeAnimation()

        #deselect all
        bpy.ops.pose.select_all(action='DESELECT')

        #select bones that will not need animation
        for bone in bpy.context.object.data.bones:
            if ".orient" in bone.name:
                bpy.context.object.data.bones[bone.name].select = True
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #force edit mode
        StateUtility.SetEditMode()

        #remove .orient bones
        armature = bpy.context.object.data
        for bone in bpy.context.object.data.bones:
            if ".orient" in bone.name:
                armature.edit_bones.remove(armature.edit_bones[bone.name])

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        #select all base armature
        bpy.ops.pose.select_all(action='SELECT')
        bpy.ops.pose.bone_layers(layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))


        #hide all layers except the first one
        for i in range(1,32):
            bpy.context.object.data.layers[i] = False

        #remove controller shapes if still in the scene
        obj = bpy.data.objects
        try :
            obj.remove(obj["Square"], do_unlink=True)
        except:
            print("no Square to delete in the scene")
        try :
            obj.remove(obj["Circle"], do_unlink=True)
        except:
            print("no Circle to delete in the scene")
        try :
            obj.remove(obj["Octagon"], do_unlink=True)
        except:
            print("no Octagon to delete in the scene")