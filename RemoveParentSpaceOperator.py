import bpy
from . RemoveParentSpace import RemoveParentSpaceUtils

class RemoveParentSpaceOperator(bpy.types.Operator):
    bl_idname = "view3d.remove_parent_space_operator"
    bl_label = "Simple operator"
    bl_description = "Removes selected .parent.rig controllers and it's related controllers and bakes affected controllers"

    def execute(self, context):
        RemoveParentSpaceUtils.RemoveParentSpace(self, context)
        return {'FINISHED'}