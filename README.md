# RigOnTheFly

Rig on the Fly is a Blender 2.80 dynamic rigging tool used to simplify and speed up animation workflow.  
Inspired by Richard Lico's GDC 2018 talk: [Animating Quill: Creating an Emotional Experience](https://www.youtube.com/watch?v=u3CzLVpuE4k&t=2011s) and his Animation Sherpa Space Switching course.  
The tool pairs very well with another one called [AnimAide](https://github.com/aresdevo/animaide).

Rig on the Fly is the result of my free time making animation tools that I feel the need for, for my work. If you have any questions or suggestions you can contact me through twitter @Wardl_ .

![WalkShowcase](images/walkShowcase.gif)

### Compatibility
Blender 2.80

### Known Issues
The Restore Parent may restore unwanted .parents and .child controllers.  
Does not support Action layers.

### Future Features
Aim with offset.

### Instalation
Go to the following address [https://gitlab.com/dypsloom/rigonthefly](https://gitlab.com/dypsloom/rigonthefly) and download the addon to a chosen folder on your computer.   

Then in Blender, go to "Add-ons" tab on the preference window. There click on the "Install" button and navigate to the folder where you downloaded the addon. Choose the RigOnTheFly file and it will be installed.

Make sure the addon check-mark is active.

Once installed you can find the RigOnTheFly panel on the right side of your 3d view in Sidebar. Press N to Toggle Sidebar visibility or click on the small arrow pointing left on the top right part of your 3d view.

At the moment RigOnTheFLy has the following features. 

### Rig & Bake

## Rig Skeleton

Adds basic FK controllers to a selected armature. Keeping any animation keys it had.

![RigSkeleton](images/RigOnSkeleton.gif)

## Bake Rig

Applies the rig's animation on to the base armature and removes all controllers created by RigOnTheFly. Usually used once you are ready to export your animation but can also be used to reset the rig's controls.

![BakeRig](images/BakeOnSkeleton.gif)

## Orient Rig/Bake

In case your armature was imported from another 3d software use **Orent Rig** to make it compatible with **RigOnTheFLy**. If your imported armature does not have animations yet, make sure to key all bones once before pressing **Orient Rig** to prevent unwanted rotations.
**Bake Orient** is the equivalent to **Bake Rig** but for armatures that used **Orient Rig** first.

**Rig Skeleton** and **Orient Rig* both key the rig into bind pose (T-pose) at frame 0 so that the whole RigOnTheFly tool works correctly. Animate from frame 1 and onwards to avoid issues.

![OrientRigBake](images/OrientRig.gif)

The character and rig of Baqir (the red panda) is from Richard Lico's @Foofinu Animation Sherpa's course. Character model made by LeoOgawaLillrank @LeoOgawa.

### Controller Size

Increase or decrease selected controllers' size to your liking. Does not affect animation in any way.

![ControllerSize](images/ControllerSize2.gif)

### IK FK Switch

Switches selected controllers to work in Inverse Kinematic/IK with pole tragets. (Two limbs IK only). 
To switch back to FK, simply select the desired IK handles and press the FK button.
Press **Shift IK Pole Angle** if the elbow or knee is facing in the wrong direction. Goes by 90° increments.

![IKFKSwitch](images/IKFK3.gif)

### Rotation and Scale Tools

## Rotation Mode

Changes selected controllers rotation mode without modifying the animation.

## Distribute

Distributes rotation between the first and last selected controllers without modifying the existing animation. *Disables translation of top controller.
After selecting the top controller, press **Apply** to return to how it was.

![Distribute](images/RotationDistribution.gif)

## Inherit Rotation/Scale

Pressing **On** enables selected controllers to inherit rotation/scale transforms from their parents. Pressing **Off** disables it.

![InheritRotationScale](images/InheritRotationScaleSwitch2.gif)

### Extra Controller

Adds a world space crosshair controller at the center of your scene.
Delete removes **any** selected controller. (careful not to remove controllers you need)

![Extra](images/AddExtra.gif)

### Space Switch

## World Transform

Chanages selected controllers to function in world space. So for example, putting their location to (0,0,0) will put them at the center of the scene regardless of if the Armature object is placed somewhere else. They are also no longer affected by their parent.
Pressing **Remove** will restore the seleced controllers.

![WorldTransform](images/WorldTransform.gif)

## World Aim/Stretch

Changes the X and Z rotation of selected controllers to aim at a world space locator. Y rotation is still editable.
Change the value of **Distance** for the space locator to be placed closer or futher once you you press **World Aim** or **World Stretch**.
**World Aim** affects only rotation as **World Stretch** affects scale as well.

![AimStretchWorld](images/AimStretchWorld.gif)

## Aim/Stretch Chain

**Aim Chain** and **Stretch Chain** changes selected controllers to rotate aiming/stretching down the hierarchy of selected bones and switches their position to world space.

![AimStretchChain](images/AimStretchChain.gif)

## Parent

Parent selected controllers to the active controller.

![Parent](images/Parent.gif)

## Parent Copy

Parent selected controllers to a copy of the active controller. Contrary to **Parent**, **Parent Copy** lets you parent to motion down the hierarchy. Ideal for ball foot/toe pivot or three limbs IK.

![ParentCopy](images/ParentCopy.gif)

### Inertia Transforms

Applies **Inertia Value** to the X,Y and Z of the location, rotation or scale of selected controllers. 
Higher **Inertia Value** will make the controllers take longer to change dirrection.

![Inertia](images/Inertia.gif)

