import bpy
from . Utility import StateUtility

class RemoveParentSpaceUtils:

    def RemoveParentSpace (self, context):

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        selectedBoneP = bpy.context.selected_pose_bones.copy()
        selectedBoneP.sort(key = lambda x:len(x.parent_recursive))

        #find parent bones with the suffix ".parent.rig"
        parentBonesN = []
        for boneP in selectedBoneP:
            if ".parent.rig" in boneP.name:
                parentBonesN.append(boneP.name)
            #find parent bones through related .child
            if ".child.rig" in boneP.name:
                parentBoneN = boneP.parent.name
                if parentBoneN not in parentBonesN:
                    parentBonesN.append(parentBoneN)

        #find child bones related to parent bones found previously
        for parentBoneN in parentBonesN:
            #deselects all
            bpy.ops.pose.select_all(action='DESELECT')

            childrenBonesP = bpy.context.object.pose.bones[parentBoneN].children
            childrenBonesN = []
            for childBoneP in childrenBonesP:
                childrenBonesN.append(childBoneP.name)

            #show hidden bone layer 3 to access hidden rig bones
            bpy.context.object.data.layers[2] = True

            #select child bones for future baking
            for childBoneN in childrenBonesN:
                bpy.context.object.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
            
            #adds parent bone to the baking bones selection
            bpy.context.object.data.bones[parentBoneN.replace(".parent.rig",".rig")].select = True

            #return affected rig bones to the main layer 2
            bpy.ops.pose.bone_layers(layers=(False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
            #bake animation on selection and remove constraints
            StateUtility.BakeAnimation()

            #show hidden bone layer 3 to access hidden rig bones
            bpy.context.object.data.layers[2] = False

            #remove selected .parent.rig and related .child.rig bones
            bonesToRemove = []
            for boneN in childrenBonesN:
                bonesToRemove.append(boneN)
            bonesToRemove.append(parentBoneN)

            #deselects all
            bpy.ops.pose.select_all(action='DESELECT')

            for boneN in bonesToRemove:
                bpy.context.object.data.bones[boneN].select = True
            #clear all key frames of selected bones
            bpy.ops.anim.keyframe_clear_v3d()

            
            StateUtility.SetEditMode()
            armature = bpy.context.object.data
            for boneN in bonesToRemove:
                armature.edit_bones.remove(armature.edit_bones[boneN])

            #force pose mode
            bpy.ops.object.mode_set(mode='POSE')

            #select child bones
            for childBoneN in childrenBonesN:
                bpy.context.object.data.bones[childBoneN.replace(".child.rig",".rig")].select = True
            
            #adds parent bone to selection
            bpy.context.object.data.bones[parentBoneN.replace(".parent.rig",".rig")].select = True

