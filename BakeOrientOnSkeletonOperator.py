import bpy
from . BakeOrientOnSkeleton import BakeOrientOnSkeletonUtils

class BakeOrientOnSkeletonOperator(bpy.types.Operator):
    bl_idname = "view3d.bake_orient_on_skeleton_operator"
    bl_label = "Simple operator"
    bl_description = "Bake oriented animation on the base skeleton and remove controllers"

    def execute(self, context):
        BakeOrientOnSkeletonUtils.BakeOrientOnSkeleton(self, context)
        return {'FINISHED'}