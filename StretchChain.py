import bpy
from . Utility import StateUtility

class StretchChainUtils:
    
    @staticmethod
    def RemoveEditableBone(boneToRemove):
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in armature.edit_bones:
            if bone.name == boneToRemove.name:
                armature.edit_bones.remove(bone)

    def ChangeToStretchChain (self, context):

        #force edit mode
        StateUtility.SetEditMode()

        selectedRigBonesListE = list(bpy.context.selected_editable_bones)
        selectedRigBonesListE.sort(key = lambda x:len(x.parent_recursive))
        selectedRigBoneNameList = list()
        for selectedBone in selectedRigBonesListE:
            selectedRigBoneNameList.append(selectedBone.name)
        
        #duplicate rig bones to be used as aim bones
        bpy.ops.armature.duplicate()
       
        #add .aim.rig suffix to duplicate bones now known as aim bones.
        for copiedBones in bpy.context.selected_editable_bones:
            copiedBones.name = copiedBones.name.replace(".rig.001",".aim.rig")

            #find the matrix coordinates of the armature object
            armatureMatrix = bpy.context.object.matrix_world
            #invert armature's matrix to find where global(0,0,0) is in relation to the armature's position/roation
            armatureMatrixInvert= armatureMatrix.copy()
            armatureMatrixInvert.invert()
            #set aim bone position to global (0,0,0) with axis following world's
            copiedBones.matrix = armatureMatrixInvert

        #unparent aim bones for world Chain translation
        aimBonesListE = bpy.context.selected_editable_bones.copy()
        aimBoneNameList = list()
        for aimBone in aimBonesListE:
            aimBoneNameList.append(aimBone.name)
            aimBone.parent = None


        #armature set to pose mode
        bpy.ops.object.mode_set(mode='POSE')


        aimBonesListN = list()
        for bone in selectedRigBoneNameList:
            aimBonesListN.append(bone.replace(".rig",".aim.rig"))
        ikBonesListN = aimBonesListN[:-1]
        ikSubtargetListN = aimBonesListN[1:]


        #change rig bones' display to square, rotation mode to euler YZX and adds copy transform constraint to copy the rig bones animation.
        for aimBoneN in aimBonesListN:
            aimBone = bpy.context.object.pose.bones[aimBoneN]
            aimBone.custom_shape = bpy.data.objects["Square"]
            bpy.context.object.data.bones[aimBoneN].show_wire = True
            aimBone.rotation_mode = 'YZX'
            copyTransforms = aimBone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = aimBoneN.replace(".aim.rig",".rig")

        #bake animation on selection and remove constraints
        StateUtility.BakeAnimation()

        #make bone list "aim" at their child with IK constraint
        for i in range(len(ikBonesListN)):
            ikBones = bpy.context.object.pose.bones[ikBonesListN[i]]            
            stretchConstraint = ikBones.constraints.new('STRETCH_TO')
            stretchConstraint.target = bpy.context.object
            stretchConstraint.subtarget = ikSubtargetListN[i]
            stretchConstraint.keep_axis = 'SWING_Y'

        #make rig bones follow coresponding aim bones
        for aimBoneN in aimBonesListN:
            copyTransforms = bpy.context.object.pose.bones[aimBoneN.replace(".aim.rig",".rig")].constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = aimBoneN

        #select rig bones to move them to hidden bone layer 3
        bpy.ops.pose.select_all(action='DESELECT')
        for rigBoneN in selectedRigBoneNameList:            
            bpy.context.object.data.bones[rigBoneN].select = True
        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()
        bpy.ops.pose.bone_layers(layers=(False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

        #end script with new aim bones selected
        bpy.ops.pose.select_all(action='DESELECT')
        for aimBoneN in aimBonesListN:
            bpy.context.object.data.bones[aimBoneN].select = True
        