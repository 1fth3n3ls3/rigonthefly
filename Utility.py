import bpy
from mathutils import Matrix, Euler, Vector
from typing import NamedTuple
from bpy_extras.io_utils import axis_conversion

class StateUtility:

    @staticmethod
    def SetEditMode (toggleMirror=False):
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.context.object.data.use_mirror_x = toggleMirror


    @staticmethod
    def SaveState ():
        mode = bpy.context.mode
        selectionEditableBones = bpy.context.selected_editable_bones
        selectionPoseBones = bpy.context.selected_pose_bones
        return StateData(mode,selectionEditableBones,selectionPoseBones)

    @staticmethod
    def RecoverState (stateData):
        bpy.ops.object.mode_set(mode= stateData.mode)
    
    @staticmethod
    def RemoveEditableBone(boneToRemove):
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in armature.edit_bones:
            if bone.name == boneToRemove.name:
                armature.edit_bones.remove(bone)

    @staticmethod
    def BoneListToNameList(boneList):
        nameList = []
        for b in boneList:
            nameList.append(b.name)
        return nameList       

    @staticmethod
    def TempBoneCopySelectedBones():
        #force edit mode
        StateUtility.SetEditMode()

        #list selected bones in edit mode
        selectedBonesListE = bpy.context.selected_editable_bones.copy()
        
        #list selected bones' names
        selectedBonesListN = []
        for b in selectedBonesListE:
            selectedBonesListN.append(b.name)
        
        #duplicate base armature. Duplicate bones are selected from this operation.
        bpy.ops.armature.duplicate()

        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')
        #add copy transform constrain to duplicated bones
        for bone in bpy.context.selected_pose_bones:
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = bone.name.replace(".rig.001",".rig")

        StateUtility.BakeAnimation()
        
        return selectedBonesListN

    @staticmethod
    def SelectedBonesCopyTempBones(selectedBonesListN):
        #deselect copied bones
        bpy.ops.pose.select_all(action='DESELECT')

        #select original bone list
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone].select = True

        #make selected bones follow duplicated bones with copy transform constraint
        for bone in bpy.context.selected_pose_bones:
            copyTransforms = bone.constraints.new('COPY_TRANSFORMS')
            copyTransforms.target = bpy.context.object
            copyTransforms.subtarget = bone.name.replace(".rig",".rig.001")

        StateUtility.BakeAnimation()

        #deselect all to
        bpy.ops.pose.select_all(action='DESELECT')
        #select bones to remove to remove their keyframes first
        for bone in selectedBonesListN:
            bpy.context.object.data.bones[bone.replace(".rig",".rig.001")].select = True

        #clear all key frames of selected bones
        bpy.ops.anim.keyframe_clear_v3d()

        #remove copied bones
        StateUtility.SetEditMode()
        armature = bpy.context.object.data
        for bone in selectedBonesListN:
            armature.edit_bones.remove(armature.edit_bones[bone +".001"])
        
        #force pose mode
        bpy.ops.object.mode_set(mode='POSE')

        for rigBone in selectedBonesListN:
            bpy.context.object.data.bones[rigBone].select = True

        return selectedBonesListN

    @staticmethod
    def BakeAnimationWithOptions(bakeOptions):

        #bake copied bones animation so that they have the same animation as the base armature.
        bpy.ops.nla.bake(
            frame_start=bakeOptions.frame_start, 
            frame_end=bakeOptions.frame_end, 
            only_selected=bakeOptions.only_selected, 
            visual_keying=bakeOptions.visual_keying, 
            clear_constraints=bakeOptions.clear_constraints,
            clear_parents=bakeOptions.clear_parents,
            use_current_action=bakeOptions.use_current_action, 
            bake_types={'POSE'}
        )

    @staticmethod
    def BakeAnimation():
        bakeOptions=BakeOptions()
        
        #bpy.context.active_object.animation_data.action.frame_range

        bakeOptions.frame_start=bpy.context.active_object.animation_data.action.frame_range.x
        bakeOptions.frame_end=bpy.context.active_object.animation_data.action.frame_range.y

        StateUtility.BakeAnimationWithOptions(bakeOptions)

class BakeOptions():
    mode:str
    frame_start=0
    frame_end=1
    only_selected=True
    visual_keying=True
    clear_parents=False
    clear_constraints=True

    use_current_action=True
    bake_types={'POSE'}

class StateData(NamedTuple):
    mode:str
    selectionEditableBones:bpy.types.EditBone
    selectionPoseBones:bpy.types.PoseBone